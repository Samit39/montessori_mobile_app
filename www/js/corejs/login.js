//Hosts Declerations
var rootHost="http://192.168.100.3:8080/";
var APIDistributor="monte_api_distributer/";

var appid="764df61a8dcd29d91207056e44f77a4714a13688";
var checkLoginURL=rootHost+APIDistributor+"API_Login/checklogin";
var getSchoolDetailsURL=rootHost+APIDistributor+"API_Login/getSchoolDetails";
var getTeacherInfoURL=rootHost+APIDistributor+"API_Login/getTeacherInfo";
//URL DECLERATIONs

$(document).ready(function()
{
    // Make your jQuery Mobile framework configuration changes here!
    $.mobile.allowCrossDomainPages = true;
    $.mobile.ajaxEnabled = true;
    $.mobile.pushStateEnabled = false;
    $.support.cors = true;
    $("#btn_signin").click(signinclickfunction);
}); //document ready


	// $("#btn_signin").click(function()
	function signinclickfunction()
	{

		var username=$("#username").val();
		var password=$("#password").val();
		if(!username || !password)
		{	//event.preventDefault(); 
			swal("Oops!", "Please Enter Valid Username And Password", "error");
			// sweetAlert('Congratulations!', 'Your message has been successfully sent', 'success');
		}
		else
		{
			var dataString="username="+username+"&password="+password+"&appid="+appid;
			$.ajax({
				type: "GET",
				url:checkLoginURL,
				data: dataString,
				dataType:"json",
				async:false,
				crossDomain: true,
				cache: false,
				processData:false,
				contenttype: "application/x-www-form-urlencoded",
				success: function(data){
				// var obj = JSON.parse(data);
				if(data.status==200)
				{	
					$.each(data.result, function(i, field){
						if(field.access_type==2)
						{
							//Teacher le login gareko
							storeSchoolDataInStorage();
							storeTeacherDataInStorage(field.id);
							
						}
						else if(field.access_type==3)
						{
							//Parents or Student le login gareko
						}
					});
				}
				else if (data.status==404)
				{
					// event.preventDefault();
					swal("Oops!", "Incorrect Username Or Password", "error");
					// window.location.href="login.html";
				}
				else if (data.status==401)
				{
					// event.preventDefault(); 
        			// swal("Oops!", "Invalid Request. Please Try Again Later", "error");
        			// window.location.href="login.html";
        			setTimeout(function () { 
        				swal({
        					title: "Oops!",
        					text: "Invalid Request. Please Try Again Later",
        					type: "error",
        					confirmButtonText: "OK"
        				},
        				function(isConfirm){
        					if (isConfirm) {
        						window.location.href = "login.html";
        					}
        				}); }, 1000);
        		}
        		else
        		{
					// event.preventDefault(); 
        			// swal("Oops!", "Unknown Error Occured. Please Try Again Later", "error");
        			// window.location.href="login.html";
        			setTimeout(function () { 
        				swal({
        					title: "Oops!",
        					text: "Unknown Error Occured. Please Try Again Later",
        					type: "error",
        					confirmButtonText: "OK"
        				},
        				function(isConfirm){
        					if (isConfirm) {
        						window.location.href = "login.html";
        					}
        				}); }, 1000);	
        		}
        	},
        	error:function (jqXHR, textStatus, errorThrown){
        		swal("Oops!", "There was an error processing your request. Please try again.", "error");
        		// swal("Oops!", JSON.stringify(jqXHR), "error");	
        	}
        });
		}

		
	}//submit click

	function storeSchoolDataInStorage()
	{
		console.log("enteres");
		var dataString="appid="+appid;
		$.ajax({
			type: "GET",
			url:getSchoolDetailsURL,
			data: dataString,
			dataType:"json",
			async:false,
			crossDomain: true,
			cache: false,
			processData:false,
			contenttype: "application/x-www-form-urlencoded",
			success: function(data){
				if(data.status==401)
				{
        			setTimeout(function () { 
        				swal({
        					title: "Oops!",
        					text: "Invalid Request.[Unable To Fetch School Details] Please Try Again Later",
        					type: "error",
        					confirmButtonText: "OK"
        				},
        				function(isConfirm){
        					if (isConfirm) {
        						window.location.href = "login.html";
        					}
        				}); }, 1000);	

        		}
        		else if(data.status==404)
        		{
					setTimeout(function () { 
						swal({
							title: "Oops!",
							text: "Incorrect Credentials!! [Unable To Fetch School Details]",
							type: "error",
							confirmButtonText: "OK"
						},
						function(isConfirm){
							if (isConfirm) {
								window.location.href = "login.html";
							}
						}); }, 1000);	
				}
				else if(data.status==200)
				{
					$.each(data.result, function(i, field){
						localStorage.setItem("schoolname",field.school_name);
					});
					console.log(localStorage.schoolname);
				}
				else
				{
        			setTimeout(function () { 
        				swal({
        					title: "Oops!",
        					text: "Unknown Error Occured. Please Try Again Later!!. [Unable To Fetch School Details]",
        					type: "error",
        					confirmButtonText: "OK"
        				},
        				function(isConfirm){
        					if (isConfirm) {
        						window.location.href = "login.html";
        					}
        				}); }, 1000);	
        		}

        	},
        	error:function (jqXHR, textStatus, errorThrown){
        		setTimeout(function () { 
        			swal({
        				title: "Oops!",
        				text: "There was an error processing your request. Please try again.",
        				type: "error",
        				confirmButtonText: "OK"
        			},
        			function(isConfirm){
        				if (isConfirm) {
        					window.location.href = "login.html";
        				}
        			}); }, 1000);	
        	}
        });	
	}

	function storeTeacherDataInStorage(loginid)
	{
		var dataString="appid="+appid+"&loginid="+loginid;
		$.ajax({
			type: "GET",
			url:getTeacherInfoURL,
			data: dataString,
			dataType:"json",
			async:false,
			crossDomain: true,
			cache: false,
			processData:false,
			contenttype: "application/x-www-form-urlencoded",
			success: function(data){
				if(data.status==401)
				{
					// event.preventDefault(); 
        			// swal("Oops!", "Invalid Request. Please Try Again Later", "error");
        			// window.location.href="login.html";
        			setTimeout(function () { 
        				swal({
        					title: "Oops!",
        					text: "Invalid Request. Please Try Again Later",
        					type: "error",
        					confirmButtonText: "OK"
        				},
        				function(isConfirm){
        					if (isConfirm) {
        						window.location.href = "login.html";
        					}
        				}); }, 1000);	

        		}
        		else if(data.status==404)
        		{
					// event.preventDefault();
					// swal("Oops!", "Incorrect Credentials", "error");
					// window.location.href="login.html";
					setTimeout(function () { 
						swal({
							title: "Oops!",
							text: "Incorrect Credentials!!",
							type: "error",
							confirmButtonText: "OK"
						},
						function(isConfirm){
							if (isConfirm) {
								window.location.href = "login.html";
							}
						}); }, 1000);	
				}
				else if(data.status==200)
				{
					$.each(data.result, function(i, field){
						localStorage.setItem("loggedin","true");
						localStorage.setItem("usertype","teacher");
						localStorage.setItem("fullname",field.full_name);
						localStorage.setItem("userid",field.id);
						// console.log(localStorage.getItem("userid"));
						// window.location.href="teacherfeedspage.html";
						setTimeout(function () { 
							swal({
								title: "Success!",
								text: "Logged In Successfully!!",
								type: "success",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teacherfeedspage.html";
								}
							}); }, 1000);
					});
				}
				else
				{
					// event.preventDefault(); 
        			// swal("Oops!", "Unknown Error Occured. Please Try Again Later", "error");
        			// window.location.href="login.html";
        			setTimeout(function () { 
        				swal({
        					title: "Oops!",
        					text: "Unknown Error Occured. Please Try Again Later!!",
        					type: "error",
        					confirmButtonText: "OK"
        				},
        				function(isConfirm){
        					if (isConfirm) {
        						window.location.href = "login.html";
        					}
        				}); }, 1000);	
        		}

        	},
        	error:function (jqXHR, textStatus, errorThrown){
				// console.log(error);
				// event.preventDefault(); 
        		// swal("Oops!", "There was an error processing your request. Please try again.", "error");
        		// window.location.href="login.html";
        		setTimeout(function () { 
        			swal({
        				title: "Oops!",
        				text: "There was an error processing your request. Please try again.",
        				type: "error",
        				confirmButtonText: "OK"
        			},
        			function(isConfirm){
        				if (isConfirm) {
        					window.location.href = "login.html";
        				}
        			}); }, 1000);	
        	}
        });	
	} //storing teacher data


	