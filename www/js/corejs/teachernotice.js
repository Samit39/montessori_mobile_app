//Hosts Declerations
var rootHost="http://192.168.100.3:8080/";
var APIDistributor="monte_api_distributer/";

var appid="764df61a8dcd29d91207056e44f77a4714a13688";
var addNoticeURL=rootHost+APIDistributor+"API_Teacher_Notices/addnotice";
var getAllNoticesURL=rootHost+APIDistributor+"API_Teacher_Notices/getAllNotices";
//URL DECLERATIONs

$(document).ready(function()
{
	$( document ).bind( "mobileinit", function(){
	    // Make your jQuery Mobile framework configuration changes here!
	    $.mobile.allowCrossDomainPages = true;
	    $.mobile.ajaxEnabled = true;
	    $.mobile.pushStateEnabled = false;
	    $.support.cors = true;
	});
	review_to_load_this_page();
	setAppTitle();
	$('.datepicker').pickadate(); // Data Picker Initialization
	view_all_notices();
	$("#addNewNotice").click(perform_add_new_notice_operation);
	hide_loading_show_main();
	$("#logout").click(perform_log_out_operation);
}); //document ready
//------------------------------------------------------------------------------------
function view_all_notices()
{
	var dataString="appid="+appid;
	$.ajax({
		type: "GET",
		url:getAllNoticesURL,
		data: dataString,
		dataType:"json",
		// async:false,
		crossDomain: true,
		cache: false,
		processData:false,
		contentType: "application/x-www-form-urlencoded",
		success: function(data){
			// console.log(JSON.stringify(data));
			if(data.status==200)
			{
				$.each(data.result, function(i, field){
				//date show garne operation
				var datefromdatabase=field.published_date;
				var splitonspace=datefromdatabase.split(" ");
				var res=splitonspace[0].split("-");
				var month=getMonth(res[1]);
				var datetoshow=res[2]+" "+month+", "+res[0];
				//
				$("#noticeView").append('<div class="col-lg-4 col-md-12 mb-4"><div class="card card-cascade wider reverse w-100"> <!-- Card content --> <div class="card-body card-body-cascade text-center"> <!-- Title --> <h4 class="card-title"><strong>Notice!! Notice!!</strong></h4> <!-- Text --> <h5><p class="card-text">'+field.notice_description+'</p></h5> <!-- Posted By --> <a class="px-2">Notice Posted On:-'+datetoshow+'</a><br><a class="px-2">Notice Posted By:-'+field.full_name+'</a><br><a class="px-2">Notice Ends On:-'+field.end_date+'</a> </div> </div> </div>');
				});
			}
				
		},
		error:function (jqXHR, textStatus, errorThrown){
			swal("Oops!", "There was an error processing your request. Please try again.", "error");
        	}
        });
}

function getMonth(monthid)
{
	switch (monthid) {
    case "01":
        return "January";
        break;
    case "02":
        return "February";
        break;
    case "03":
        return "March";
        break;
    case "04":
        return "April";
        break;
    case "05":
        return "May";
        break;
    case "06":
        return "June";
        break;
    case "07":
        return "July";
        break;
    case "08":
        return "August";
        break;
    case "09":
        return "September";
        break;
    case "10":
        return "October";
        break;
    case "11":
        return "November";
        break;
    case "12":
        return "December";
        break;
    default:
    return "Error";
	}
}
function perform_add_new_notice_operation()
{
	show_loading_hide_main();
	// console.log("clicker");
	var noticeDesc=$("#noticeDescription").val();
	var teacherid=localStorage.userid;
	var noticeExpiryDate=$("#noticeExpiryDate").val();
	// console.log(noticeDesc+teacherid+noticeExpiryDate);
	if(notice_form_validation(noticeDesc,teacherid,noticeExpiryDate))
	{
		// console.log("form validated");
		data = new FormData();
		data.append('appid',appid);
		data.append('teacherid',teacherid);
		data.append('notice_desc',noticeDesc);
		data.append('notice_expiry_date',noticeExpiryDate);
		//
		// To display the key/value pairs of just prepared data to send
		// for (var pair of data.entries()) {
		//     console.log(pair[0]+ ', ' + pair[1]); 
		// }
		//
		$.ajax({
				type: "POST",
				url:addNoticeURL,
				data: data,
				enctype: 'multipart/form-data',
				async:false,
				crossDomain: true,
				cache: false,
				processData:false,
				contentType:false,

				success: function(returneddata){
					// console.log(JSON.parse(returneddata));
					// console.log(JSON.stringify(returneddata));
					if(JSON.parse(returneddata).status==200)
					{
						setTimeout(function () { 
							swal({
								title: "Success!",
								text: "Notice Added Successfully!!",
								type: "success",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teachernotice.html";
								}
							}); }, 1000);
					}
					else if(JSON.parse(returneddata).status==404)
					{
						setTimeout(function () { 
							swal({
								title: "Oops!",
								text: "Invalid Parameters Sent, Pleae Try Again!!",
								type: "error",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teachernotice.html";
								}
							}); }, 1000);	
					}
					else if(JSON.parse(returneddata).status==401)
					{
						setTimeout(function () { 
							swal({
								title: "Oops!",
								text: "Unauthorized API Key",
								type: "error",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teachernotice.html";
								}
							}); }, 1000);
					}
				},
				error:function (jqXHR, textStatus, errorThrown){
					swal("Oops!", "There was an error processing your request. Please try again.", "error");
	        		// swal("Oops!", JSON.stringify(jqXHR), "error");	
	        	}
	        });
	}
}

function notice_form_validation(noticeDesc,teacherid,noticeExpiryDate)
{
	if(!noticeDesc)
	{
		swal("Oops!", "You Must Provide Desctiption For This Notice!!", "error");
	}
	else if(!noticeExpiryDate)
	{
		swal("Oops!", "You Must Pick Notice Expiry Date!!", "error");
	}
	else
	{
		return true;
	}
}
//-----------------------------------------------------------------------------
function setAppTitle()
{
	$("#apptitle").html(localStorage.schoolname);
}
function hide_loading_show_main()
{
	$("#mainbody").show();
	$("#loadingwindow").hide();	
}
function show_loading_hide_main()
{
	$("#mainbody").hide();
	$("#loadingwindow").show();
}
function review_to_load_this_page()
{
	if(localStorage.loggedin=="true" && localStorage.usertype=="teacher")
	{
		// sweetAlert('Success!!', 'Log In Completed Successfully!!', 'success');
	}
	else
	{
		perform_log_out_operation();
	}
}
function perform_log_out_operation()
{
	//Clear datas from local storage
	localStorage.removeItem("loggedin");
	localStorage.removeItem("usertype");
	localStorage.removeItem("fullname");
	localStorage.removeItem("userid");
	localStorage.removeItem("schoolname");

	window.location.href="login.html";
}