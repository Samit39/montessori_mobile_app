//Hosts Declerations
var rootHost="http://192.168.100.3:8080/";
var APIDistributor="monte_api_distributer/";

var appid="764df61a8dcd29d91207056e44f77a4714a13688";
var photolocation=rootHost+APIDistributor+"uploads/totalphotos/"; //SERVER STORAGE LOCATION
var getAllChatListURL=rootHost+APIDistributor+"API_Teacher_Chat/getAllRunningClassStudentsAndTeachers";
var getSingleStudentDetails=rootHost+APIDistributor+"API_Teacher_Chat/getSingleStudentDetailsFromId";
var getTeacherChatDetails=rootHost+APIDistributor+"API_Teacher_Chat/getTeacherChatDetails";
//URL DECLERATIONs

var senderId;
var receiverId;
var senderName="";
var receiverName="";
$(document).ready(function()
{
	$( document ).bind( "mobileinit", function(){
	    // Make your jQuery Mobile framework configuration changes here!
	    $.mobile.allowCrossDomainPages = true;
	    $.mobile.ajaxEnabled = true;
	    $.mobile.pushStateEnabled = false;
	    $.support.cors = true;
	});
	review_to_load_this_page();
	setAppTitle();
	// prepare_classes_buttons();
	prepare_chat_list();
	hide_loading_show_main();
	$("#logout").click(perform_log_out_operation);
}); //document ready
//------------------------------------------------------------------------------------
function prepare_chat_list()
{
	var dataString="appid="+appid+"&teacherid="+localStorage.userid;
	$.ajax({
		type: "GET",
		url:getAllChatListURL,
		data: dataString,
		dataType:"json",
		// async:false,
		crossDomain: true,
		cache: false,
		processData:false,
		contentType: "application/x-www-form-urlencoded",
		success: function(data){
			// console.log(JSON.stringify(data));
			if(data.status==200)
			{
				// console.log("Success");
				//MDB Design ma Select Underlying Hune Vayeko Le garda
					//Step-1: Destroy MDB Select
					//Step-2: Add all Options
					//Step-3: Initialize MDB Select
					$('.mdb-select').material_select('destroy');
					$.each(data.result, function(i, field){
					// console.log("hh");
					$("#chatlist").append('<option value="'+field.student_id+'" data-icon="'+photolocation+field.profilepicture+'" class="rounded-circle">'+field.student_fullname+'-'+field.class_name+'</option>');
				});
					$('.mdb-select').material_select();
				}
				
			},
			error:function (jqXHR, textStatus, errorThrown){
				swal("Oops!", "There was an error processing your request. Please try again.", "error");
			}
		});
}

function closeModal()
{
	$(document).ready(function(){
		setTimeout(function(){
			$("#closeModal").click();
		},1);
	});
}

function displayNameOfSenderContainer()
{
	var dataString="appid="+appid+"&studentid="+senderId;
	$.ajax({
		type: "GET",
		url:getSingleStudentDetails,
		data: dataString,
		dataType:"json",
		// async:false,
		crossDomain: true,
		cache: false,
		processData:false,
		contentType: "application/x-www-form-urlencoded",
		success: function(data){
			// console.log(JSON.stringify(data));
			if(data.status==200)
			{
				$.each(data.result, function(i, field){
					senderName=field.full_name;
					$("#nameOfSenderContainer").html('<!-- Card --> <div class="card card-cascade wider" style="background-color: #FFFFFF;"> <!-- Card image --> <div class="view view-cascade mb-3 pl-2"> <!-- Title --> <center><h6 class="card-header-title pt-1 colorfultext">'+field.full_name+'</h6></center> </div> </div> <!-- Card -->');
				});
			}

		},
		error:function (jqXHR, textStatus, errorThrown){
			swal("Oops!", "There was an error processing your request. Please try again.", "error");
		}
	});
}

function displayChatContainer()
{
	receiverName=localStorage.fullname;
	receiverId=localStorage.userid;
	var dataString="appid="+appid+"&senderid="+senderId+"&receiverid="+receiverId;
	$.ajax({
		type: "GET",
		url:getTeacherChatDetails,
		data: dataString,
		dataType:"json",
		// async:false,
		crossDomain: true,
		cache: false,
		processData:false,
		contentType: "application/x-www-form-urlencoded",
		success: function(data){
			// console.log(JSON.stringify(data));
			if(data.status==200)
			{
				$('#chatSection').html('<div class="row"> <div class="card rare-wind-gradient chat-room"> <div class="card-body"> <!-- Grid row --> <div class="row px-lg-2 px-2"> <!-- Grid column --> <div class="col-md-12 px-0"> <div class="chat-message"> <ul class="list-unstyled chat-1 scrollbar-light-blue" id="chatbody">');
				$.each(data.result, function(i, field){
					//date show garne operation
					var datefromdatabase=field.messagedate;
					var splitonspace=datefromdatabase.split(" ");
					var res=splitonspace[0].split("-");
					var month=getMonth(res[1]);
					var datetoshow=res[2]+" "+month+", "+res[0];
					if(field.sentmessage==null)
					{
						//student le patako
						$('#chatbody').append('<!--Chat Starts Here SENDER --> <li class="d-flex justify-content-between mb-4"> <img src="'+photolocation+field.studentPP+'" alt="avatar" class="avatar rounded-circle mr-2 ml-lg-3 ml-0 z-depth-1"> <div class="chat-body white p-3 ml-2 z-depth-1"> <div class="header"> <strong class="primary-font">'+senderName+'</strong> <small class="pull-right text-muted"><i class="fa fa-clock-o"></i>'+datetoshow+'</small> </div> <hr class="w-100"> <p class="mb-0">'+field.receivedmessage+'</p> </div> </li> <!--Chat Ends Here SENDER -->')
					}
					else
					{
						//teacher le patako
						$('#chatbody').append('<!--Chat Starts Here RECEIVER --> <li class="d-flex justify-content-between mb-4"> <div class="chat-body white p-3 z-depth-1"> <div class="header"> <strong class="primary-font">'+receiverName+'</strong> <small class="pull-right text-muted"><i class="fa fa-clock-o"></i>'+datetoshow+'</small> </div> <hr class="w-100"> <p class="mb-0">'+field.sentmessage+'</p> </div> <img src="'+photolocation+field.teacherPP+'" alt="avatar" class="avatar rounded-circle mr-0 ml-3 z-depth-1"> </li> <!--Chat Ends Here Receiver --> ')
					}
					
				});
				$('#chatSection').append('</ul> <div class="white"> <div class="form-group basic-textarea"> <textarea class="form-control pl-2 my-0" id="exampleFormControlTextarea2" rows="3" placeholder="Type your message here..."></textarea> </div> </div> <button type="button" class="btn btn-outline-pink btn-rounded btn-sm waves-effect waves-dark float-right">Send</button> </div> </div> <!-- Grid column --> </div> <!-- Grid row --> </div> </div> </div>');
			}

		},
		error:function (jqXHR, textStatus, errorThrown){
			swal("Oops!", "There was an error processing your request. Please try again.", "error");
		}
	});
}

function loadChatWindow()
{
	senderId=$("#chatlist").val();
	closeModal();
	displayNameOfSenderContainer();
	displayChatContainer();
	
}

function getMonth(monthid)
{
	switch (monthid) {
    case "01":
        return "January";
        break;
    case "02":
        return "February";
        break;
    case "03":
        return "March";
        break;
    case "04":
        return "April";
        break;
    case "05":
        return "May";
        break;
    case "06":
        return "June";
        break;
    case "07":
        return "July";
        break;
    case "08":
        return "August";
        break;
    case "09":
        return "September";
        break;
    case "10":
        return "October";
        break;
    case "11":
        return "November";
        break;
    case "12":
        return "December";
        break;
    default:
    return "Error";
	}
}


//-----------------------------------------------------------------------------
function setAppTitle()
{
	$("#apptitle").html(localStorage.schoolname);
}
function hide_loading_show_main()
{
	$("#mainbody").show();
	$("#loadingwindow").hide();	
}
function show_loading_hide_main()
{
	$("#mainbody").hide();
	$("#loadingwindow").show();
}
function review_to_load_this_page()
{
	if(localStorage.loggedin=="true" && localStorage.usertype=="teacher")
	{
		// sweetAlert('Success!!', 'Log In Completed Successfully!!', 'success');
	}
	else
	{
		perform_log_out_operation();
	}
}
function perform_log_out_operation()
{
	//Clear datas from local storage
	localStorage.removeItem("loggedin");
	localStorage.removeItem("usertype");
	localStorage.removeItem("fullname");
	localStorage.removeItem("userid");
	localStorage.removeItem("schoolname");

	window.location.href="login.html";
}