//Hosts Declerations
var rootHost="http://192.168.100.3:8080/";
var APIDistributor="monte_api_distributer/";

var appid="764df61a8dcd29d91207056e44f77a4714a13688";
var photolocation=rootHost+APIDistributor+"uploads/totalphotos/"; //SERVER STORAGE LOCATION
var getRunningClassURL=rootHost+APIDistributor+"API_Upload_Feed/getAllRunningClass";
var getAllPhotosURL=rootHost+APIDistributor+"API_Teacher_Gallery/getAllPhotos";
//URL DECLERATIONs

$(document).ready(function()
{
	$( document ).bind( "mobileinit", function(){
	    // Make your jQuery Mobile framework configuration changes here!
	    $.mobile.allowCrossDomainPages = true;
	    $.mobile.ajaxEnabled = true;
	    $.mobile.pushStateEnabled = false;
	    $.support.cors = true;
	});
	review_to_load_this_page();
	setAppTitle();
	prepare_classes_buttons();
	load_gallery_photos();
	hide_loading_show_main();
	$("#logout").click(perform_log_out_operation);
}); //document ready
//------------------------------------------------------------------------------------
function load_gallery_photos()
{
	var dataString="appid="+appid;
	$.ajax({
		type: "GET",
		url:getAllPhotosURL,
		data: dataString,
		dataType:"json",
		// async:false,
		crossDomain: true,
		cache: false,
		processData:false,
		contentType: "application/x-www-form-urlencoded",
		success: function(data){
			// console.log(JSON.stringify(data));
			if(data.status==200)
			{
				$.each(data.result, function(i, field){
				$("#gallery").append('<div class="mb-3 pics animation all 2"><img class="img-fluid" src="'+photolocation+field.photo_name+'" alt="Card image cap"></div>');
				});
			}
				
		},
		error:function (jqXHR, textStatus, errorThrown){
			swal("Oops!", "There was an error processing your request. Please try again.", "error");
        	}
        });
}

function prepare_classes_buttons()
{
	var dataString="appid="+appid;
	$.ajax({
		type: "GET",
		url:getRunningClassURL,
		data: dataString,
		dataType:"json",
		// async:false,
		crossDomain: true,
		cache: false,
		processData:false,
		contentType: "application/x-www-form-urlencoded",
		success: function(data){
			// console.log(JSON.stringify(data));
			if(data.status==200)
			{
				$.each(data.result, function(i, field){
				$("#classesbuttons").append('<button type="button" class="btn btn-outline-black waves-effect filter" data-rel="all">'+field.class_name+'</button>');
				});
			}
				
		},
		error:function (jqXHR, textStatus, errorThrown){
			swal("Oops!", "There was an error processing your request. Please try again.", "error");
        	}
        });
}	

//-----------------------------------------------------------------------------
function setAppTitle()
{
	$("#apptitle").html(localStorage.schoolname);
}
function hide_loading_show_main()
{
	$("#mainbody").show();
	$("#loadingwindow").hide();	
}
function show_loading_hide_main()
{
	$("#mainbody").hide();
	$("#loadingwindow").show();
}
function review_to_load_this_page()
{
	if(localStorage.loggedin=="true" && localStorage.usertype=="teacher")
	{
		// sweetAlert('Success!!', 'Log In Completed Successfully!!', 'success');
	}
	else
	{
		perform_log_out_operation();
	}
}
function perform_log_out_operation()
{
	//Clear datas from local storage
	localStorage.removeItem("loggedin");
	localStorage.removeItem("usertype");
	localStorage.removeItem("fullname");
	localStorage.removeItem("userid");
	localStorage.removeItem("schoolname");

	window.location.href="login.html";
}