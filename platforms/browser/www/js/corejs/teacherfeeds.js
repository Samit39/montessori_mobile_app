//Hosts Declerations
var rootHost="http://192.168.100.3:8080/";
var APIDistributor="monte_api_distributer/";

var appid="764df61a8dcd29d91207056e44f77a4714a13688";
var photolocation=rootHost+APIDistributor+"uploads/totalphotos/"; //SERVER STORAGE LOCATION
var getAllMyFeedsURL=rootHost+APIDistributor+"API_Teacher_View_Feeds/getAllMyFeeds";
//URL DECLERATIONs

$(document).ready(function()
{
	$( document ).bind( "mobileinit", function(){
	    // Make your jQuery Mobile framework configuration changes here!
	    $.mobile.allowCrossDomainPages = true;
	    $.mobile.ajaxEnabled = true;
	    $.mobile.pushStateEnabled = false;
	    $.support.cors = true;
	});
	review_to_load_this_page();
	setAppTitle();
	display_my_posts();
	hide_loading_show_main();
	$("#logout").click(perform_log_out_operation);
}); //document ready
//------------------------------------------------------------------------------------
function display_my_posts()
{
	var dataString="appid="+appid+"&teacherid="+localStorage.userid;
	$.ajax({
		type: "GET",
		url:getAllMyFeedsURL,
		data: dataString,
		dataType:"json",
		// async:false,
		crossDomain: true,
		cache: false,
		processData:false,
		contentType: "application/x-www-form-urlencoded",
		success: function(data){
			// console.log(JSON.stringify(data));
			if(data.status==200)
			{
				$.each(data.result, function(i, field){
				// console.log(field.post_date);
				//date show garne operation
				var datefromdatabase=field.post_date;
				var splitonspace=datefromdatabase.split(" ");
				var res=splitonspace[0].split("-");
				var res2=splitonspace[1].split(":");
				var datetoshow=res[0]+"-"+res[1]+"-"+res[2]+" "+res2[0]+":"+res2[1];
				//
				$("#feedscards").append('<div class="col-md-4 mb-4"><section><div class="card"><div class="view overlay"><img src="'+photolocation+field.photo_name+'" class="card-img-top" alt="sample"><a><div class="mask rgba-white-slight"></div></a></div><a class="btn-floating btn-action ml-auto mr-4 mdb-color lighten-3"><i class="fa fa-heart"></i></a><div class="card-body"><p class="card-text mb-0">'+field.feed_description+'</p></div><div class="mdb-color lighten-3 text-center rounded-bottom"><ul class="list-unstyled list-inline font-small mt-3"><li class="list-inline-item pr-2 white-text"><i class="fa fa-clock-o pr-1"></i>'+datetoshow+'</li></ul></div></div></section></div>');
				});
			}
				
		},
		error:function (jqXHR, textStatus, errorThrown){
			swal("Oops!", "There was an error processing your request. Please try again.", "error");
        		// swal("Oops!", JSON.stringify(jqXHR), "error");	
        	}
        });
}

//-----------------------------------------------------------------------------
function setAppTitle()
{
	$("#apptitle").html(localStorage.schoolname);
}
function hide_loading_show_main()
{
	$("#mainbody").show();
	$("#loadingwindow").hide();	
}
function show_loading_hide_main()
{
	$("#mainbody").hide();
	$("#loadingwindow").show();
}
function review_to_load_this_page()
{
	if(localStorage.loggedin=="true" && localStorage.usertype=="teacher")
	{
		// sweetAlert('Success!!', 'Log In Completed Successfully!!', 'success');
	}
	else
	{
		perform_log_out_operation();
	}
}
function perform_log_out_operation()
{
	//Clear datas from local storage
	localStorage.removeItem("loggedin");
	localStorage.removeItem("usertype");
	localStorage.removeItem("fullname");
	localStorage.removeItem("userid");
	localStorage.removeItem("schoolname");

	window.location.href="login.html";
}