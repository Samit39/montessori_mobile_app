//Hosts Declerations
var rootHost="http://192.168.100.3:8080/";
var APIDistributor="monte_api_distributer/";

var appid="764df61a8dcd29d91207056e44f77a4714a13688";
var addFeedURL=rootHost+APIDistributor+"API_Upload_Feed/addfeed";
var getAllRunningClassURL=rootHost+APIDistributor+"API_Upload_Feed/getAllRunningClass";
//URL DECLERATIONs

$(document).ready(function()
{
	$( document ).bind( "mobileinit", function(){
	    // Make your jQuery Mobile framework configuration changes here!
	    $.mobile.allowCrossDomainPages = true;
	    $.mobile.ajaxEnabled = true;
	    $.mobile.pushStateEnabled = false;
	    $.support.cors = true;
	});
	review_to_load_this_page();
	setAppTitle();
	hide_loading_show_main();
	populate_tags_select_box();
	$("#logout").click(perform_log_out_operation);
	$("#btnupload").click(perform_upload_operation);
}); //document ready

function perform_upload_operation()
{	
	var feeddesc=$('#feeddescription').val();
	var tags=$('#studenttags').val();
	var photofilename=$('#photofile').val();
	if(!feeddesc)
	{
		// alert("Must Describe This Picture!");
		swal("Oops!", "You Must Describe The Picture!", "error");
	}
	else if(tags.length === 0)
	{
		// alert("Must Select At Least One Section");
		swal("Oops!", "You Must Tag At Least One Class / Section!", "error");
	}
	else if(!photofilename)
	{
		swal("Oops!", "You Must Select An Image To Upload!", "error");
	}
	else
	{
		show_loading_hide_main();
		finalize_upload_operation(feeddesc,tags);
	}
}

function finalize_upload_operation(feeddesc,tags)
{
	data = new FormData();
	var imgname  =  $('#photofile').val();
	var size  =  $('#photofile')[0].files[0].size;
	var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
	if(ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='PNG' || ext=='JPG' || ext=='JPEG')
	{
		if(size<=7000000)
		{
	     	//Both Size And Extension Are OK
	     	var teacherid=localStorage.getItem("userid");
	     	data.append('photo_name', $('#photofile')[0].files[0]);
	     	data.append('feed_desc',feeddesc);
	     	data.append('tag_ids',tags);
	     	data.append('appid',appid);
	     	data.append('teacherid',teacherid);
		    //
		    // To display the key/value pairs of just prepared data to send
			// for (var pair of data.entries()) {
			//     console.log(pair[0]+ ', ' + pair[1]); 
			// }
			//
			$.ajax({
				type: "POST",
				url:addFeedURL,
				data: data,
				// dataType:"json",
				enctype: 'multipart/form-data',
				async:false,
				crossDomain: true,
				cache: false,
				processData:false,
				// contenttype: "application/x-www-form-urlencoded",
				contentType:false,

				success: function(returneddata){
					// console.log(JSON.parse(returneddata));
					// console.log(JSON.stringify(returneddata));
					if(JSON.parse(returneddata).status==200)
					{
						// sweetAlert('Success!!', 'Photo Uploaded Successfully!!', 'success');
						// window.location.href="teacherfeedspage.html";
						setTimeout(function () { 
							swal({
								title: "Success!",
								text: "Photo Uploaded Successfully!!",
								type: "success",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teacherfeedspage.html";
								}
							}); }, 1000);
					}
					else if(JSON.parse(returneddata).status==404)
					{
						// swal("Oops!", "Invalid Parameters Sent, Pleae Try Again!!", "error");
						// window.location.href="teacherpost.html";
						setTimeout(function () { 
							swal({
								title: "Oops!",
								text: "Invalid Parameters Sent, Pleae Try Again!!",
								type: "error",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teacherpost.html";
								}
							}); }, 1000);	
					}
					else if(JSON.parse(returneddata).status==500)
					{
						// swal("Oops!", "Upload Failed, Internal Server Error!!", "error");
						// window.location.href="teacherpost.html";
						setTimeout(function () { 
							swal({
								title: "Oops!",
								text: "Upload Failed, Internal Server Error!!",
								type: "error",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teacherpost.html";
								}
							}); }, 1000);
					}
					else if(JSON.parse(returneddata).status==415)
					{
						// swal("Oops!", "Unsupported Media Type, Please try Again!!", "error");
						// window.location.href="teacherpost.html";
						setTimeout(function () { 
							swal({
								title: "Oops!",
								text: "Unsupported Media Type, Please try Again!!",
								type: "error",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teacherpost.html";
								}
							}); }, 1000);
					}
					else if(JSON.parse(returneddata).status==204)
					{
						// swal("Oops!", "File Not Selected, Please try Again!!", "error");
						// window.location.href="teacherpost.html";
						setTimeout(function () { 
							swal({
								title: "Oops!",
								text: "File Not Selected, Please try Again!!",
								type: "error",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teacherpost.html";
								}
							}); }, 1000);
					}
					else if(JSON.parse(returneddata).status==401)
					{
						// swal("Oops!", "Unauthorized API Key", "error");
						// window.location.href="teacherfeedspage.html";
						setTimeout(function () { 
							swal({
								title: "Oops!",
								text: "Unauthorized API Key",
								type: "error",
								confirmButtonText: "OK"
							},
							function(isConfirm){
								if (isConfirm) {
									window.location.href = "teacherfeedspage.html";
								}
							}); }, 1000);
					}
				},
				error:function (jqXHR, textStatus, errorThrown){
					swal("Oops!", "There was an error processing your request. Please try again.", "error");
	        		// swal("Oops!", JSON.stringify(jqXHR), "error");	
	        	}
	        });

		    // console.log(JSON.stringify(data));
		    // console.log(data);
		    // alert(data);
		    //

		}
		else
		{
			swal("Oops!", "Image Size Exceded, Max Image Size Is 7MB!", "error");	
		}
	}
	else
	{
		swal("Oops!", "Please Select Valid Image File, Supported Format: PNG, JPG, GIF!", "error");
	}
}
//Function to fill tag select box
function populate_tags_select_box()
{
	// alert("ENTER");
	var dataString="appid="+appid;
	$.ajax({
		type: "GET",
		url:getAllRunningClassURL,
		data: dataString,
		dataType:"json",
			// async:false,
			crossDomain: true,
			cache: false,
			processData:false,
			contentType: "application/x-www-form-urlencoded",
			success: function(data){
				// console.log(JSON.stringify(data));
				if(data.status==200)
				{
					//MDB Design ma Select Underlying Hune Vayeko Le garda
					//Step-1: Destroy MDB Select
					//Step-2: Add all Options
					//Step-3: Initialize MDB Select
					$('.mdb-select').material_select('destroy');
					$.each(data.result, function(i, field){
						$('#studenttags').append('<option value="'+field.id+'">'+field.class_name+"-"+field.class_nick_name+'['+field.enrolled_year+' Intake]'+'</option>');
					});
					$('.mdb-select').material_select();
				}
				
			},
			error:function (jqXHR, textStatus, errorThrown){
				swal("Oops!", "There was an error processing your request. Please try again.", "error");
        		// swal("Oops!", JSON.stringify(jqXHR), "error");	
        	}
        });
}
//------------------------------------------------------------------------
function setAppTitle()
{
	$("#apptitle").html(localStorage.schoolname);
}
function hide_loading_show_main()
{
	$("#mainbody").show();
	$("#loadingwindow").hide();	
}
function show_loading_hide_main()
{
	$("#mainbody").hide();
	$("#loadingwindow").show();
}
function review_to_load_this_page()
{
	if(localStorage.loggedin=="true" && localStorage.usertype=="teacher")
	{
		// sweetAlert('Success!!', 'Log In Completed Successfully!!', 'success');
	}
	else
	{
		perform_log_out_operation();
	}
}
function perform_log_out_operation()
{
	//Clear datas from local storage
	localStorage.removeItem("loggedin");
	localStorage.removeItem("usertype");
	localStorage.removeItem("fullname");
	localStorage.removeItem("userid");
	localStorage.removeItem("schoolname");

	window.location.href="login.html";
}
//----------------------------------------------------------------------------